package prassee.zc

import java.io.{File, FileOutputStream}
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.ServerSocketChannel

object ZeroCopyServer extends App {

  lazy val serverPort = 9026

  start()

  def start() = {

    val socketAddress = new InetSocketAddress(serverPort)
    val listener = ServerSocketChannel.open()

    listener.socket().bind(socketAddress)
    listener.configureBlocking(true)

    println("Listening on port " + serverPort)

    val oneMb = 1024
    try {
      val bb = ByteBuffer.allocate(oneMb * 8)
      var read = 0
      var file = new File("/home/prasanna/jdk.tar.gz")
      while (true) {
        val sockChnl = listener.accept()
        val fos = new FileOutputStream(file, true)
        while (read != -1) {
          read = sockChnl.read(bb)
          fos.write(bb.array())
          bb.rewind()
        }
        if (read == -1) {
          println("end of packet")
          fos.close()
          file = null
        }
        read = 0
      }
    } catch {
      case e: Exception => e.printStackTrace()
    }
  }

}
