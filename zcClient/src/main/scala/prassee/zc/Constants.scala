package prassee.zc

import java.io.File
import java.net.InetSocketAddress

case class TransferParams(file: File, addr: InetSocketAddress)
