package prassee.zc

import java.io.{File, FileInputStream}
import java.net.InetSocketAddress
import java.nio.channels.SocketChannel

/**
 *
 */
object ZeroCopyClient extends App {

  lazy val file = new File("/data/javaPlatform/jdk-8u20-linux-x64.tar.gz")

  transfer(TransferParams(file, findHost()))

  private def findHost() = new InetSocketAddress("127.0.0.1", 9026)

  def transfer(txfrParam: TransferParams) = {
    try {
      val sSockChannel = SocketChannel.open()
      val fileTx = txfrParam.file
      sSockChannel.connect(txfrParam.addr)
      sSockChannel.configureBlocking(true)
      val fileChannel = new FileInputStream(fileTx).getChannel
      fileChannel.transferTo(0, fileTx.length(), sSockChannel)
    } catch {
      case e: Exception => e.printStackTrace()
    }
  }

}
